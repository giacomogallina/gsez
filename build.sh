#!/usr/bin/env bash

NAME="gaussian-splatting"

# update tensorflow image
docker pull nvidia/cuda:12.2.0-devel-ubuntu22.04

docker build . -t $NAME --progress=plain

