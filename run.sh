#!/bin/bash

FPS=$1

if [ -f /data/input/0001.jpg ]
then
  echo "images appear to be present .."
else
  ffmpeg -i /data/video -qscale:v 1 -qmin 1 -vf fps=$FPS /data/input/%04d.jpg
fi


#python3.10 /gs/convert.py -s /data/ --camera SIMPLE_RADIAL --no_gpu #GPU produces worse results (?)
# convert.py gave me problems, produced only 2 images
colmap feature_extractor --database_path /data/database.db --image_path /data/input
colmap exhaustive_matcher --database_path /data/database.db
mkdir -p /data/sparse
colmap mapper --database_path /data/database.db --image_path /data/input --output_path /data/sparse
colmap image_undistorter --image_path /data/input --input_path /data/sparse/0/ --output_path /data
mv /data/sparse/*.bin /data/sparse/0/ # i have no idea why this is done

#training
python3.10 /gs/train.py \
    -s /data/ \
    --iterations 15000 \
    --data_device cpu \
    --model_path=/data/output/ \
    --save_iterations 5000 10000 15000 \
    --test_iterations -1

# python3.10 /gs/3dgsconverter/3dgsconverter.py -i /data/output/point_cloud/iteration_20000/point_cloud.ply -o /data/output/point_cloud/iteration_20000/output_cc.ply -f cc --rgb --density_filter --remove_flyers
python3.10 /gs/3dgsconverter/3dgsconverter.py -i /data/output/point_cloud/iteration_15000/point_cloud.ply -o /data/output/point_cloud/iteration_15000/output_cc.ply -f cc --rgb --remove_flyers
python3.10 /gs/3dgsconverter/3dgsconverter.py -i /data/output/point_cloud/iteration_15000/output_cc.ply -o /data/output/point_cloud/iteration_15000/point_cloud_clean.ply -f 3dgs

exit 0
