#!/bin/env python3

import argparse 
import os
import sys
import shutil
import subprocess

args = argparse.ArgumentParser(description = "3D Gaussian Splatting made Easy")

args.add_argument("input", help = "the input video or image folder")
args.add_argument("-o", "--output", help = "path of the output, 'INPUT-out' by default")
args.add_argument("-f", "--fps", type = int, default = 2,
                  help = "framerate at which input videos are downsampled, 2 by default")
args.add_argument("-m", "--manual", action="store_true",
                  help = "after starting the container where all the work happens, runs bash\
                  instead of /bin/run.sh")

args = args.parse_args()
print(args)

args.input = os.path.abspath(args.input)
print(args.output)
if args.output is None:
    args.output = args.input + "-out"
else:
    args.output = os.path.abspath(args.output)

print(args)

if not os.path.exists(args.input):
    print("Error: can't find", args.input)
    sys.exit(1)
    
output_input = os.path.join(args.output, "input")
output_output = os.path.join(args.output, "output")
os.makedirs(args.output, exist_ok = True)
os.makedirs(output_input, exist_ok = True)
os.makedirs(output_output, exist_ok = True)

if os.path.isfile(args.input):
    subprocess.run(["ffmpeg", "-i", args.input, "-qscale:v", "1", "-qmin", "1", "-vf", f"fps={args.fps}", output_input+"/%04d.jpg"])
    # shutil.copy(args.input, os.path.join(args.output, "video"))
elif os.path.isdir(args.input):
    for (i, f) in enumerate(os.listdir(args.input)):
        # print(i, f)
        # print(os.path.splitext(f))
        subprocess.run(["ffmpeg", "-i", os.path.join(args.input, f), "-vf", "scale=min(iw\,2400):-1", os.path.join(output_input, f"{i+1:04d}.jpg")])
        # if os.path.splitext(f)[1] == ".jpg":
            # shutil.copy(os.path.join(args.input, f), os.path.join(output_input, f"{i+1:04d}.jpg"))
        # else:
            # print("please provide images in jpg format")
            # sys.exit(1)
else:
    print(args.input, "is neither a file nor a directory")
    sys.exit(1)


subprocess.run([
"docker", "run",
	"--rm",
	"--privileged",
	"--gpus", "all",
	"--shm-size", "8G",
	"-it",
	"-v", f"{args.output}:/data",
    "-e", f"FPS={args.fps}",
	"--name", "gaussian-splatting-container",
	"-p", "6009:6009",
	"gaussian-splatting", "bash" if args.manual else "run.sh",
])

