#FROM tensorflow/tensorflow:latest-gpu
FROM nvidia/cuda:12.2.0-devel-ubuntu22.04

ENV DEBIAN_FRONTEND noninteractive

#ARG user_id
#ARG root_psw="12345678"
#ARG user_psw="ok"
#ARG user_name=user

# Installs the necessary pkgs.
RUN \
	echo "**** packages installation ****" \
		&& apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/3bf863cc.pub \
		&& apt-get update && apt-get dist-upgrade -y && apt-get install -y \
			vim \
            build-essential \
            cmake \
            ffmpeg \
            imagemagick \
            unzip \
            freeglut3-dev \
            libopencv-dev \
            libjpeg-dev \
            libpng-dev \
            libglew-dev \
            libpthread-stubs0-dev \
			git \
			virtualenv \
            sqlite3 \
            libsqlite3-dev \
			time \
			sudo \
			wget \
			nano \
            libboost-program-options-dev \
            libboost-filesystem-dev \
            libboost-graph-dev \
            libboost-regex-dev \
            libboost-system-dev \
            libboost-test-dev \
            libeigen3-dev \
            libsuitesparse-dev \
            libfreeimage-dev \
            libgoogle-glog-dev \
            libgflags-dev \
            libglew-dev \
            qtbase5-dev \
            libqt5opengl5-dev \
            libcgal-dev \
            libcgal-qt5-dev \
            libmetis-dev \
            libflann-dev \
            libatlas-base-dev \
            libsuitesparse-dev \
            libcufft10 \
            libcusparse11 \
            libcublas11 \
            libcublaslt11 \
	    nvidia-cuda-toolkit \
	    libceres-dev \
	    ninja-build \
	&& echo "**** python pip update ****" \
		&& /usr/bin/python3 -m pip install --upgrade pip


RUN mkdir /gs && mkdir /data
WORKDIR /gs


RUN cd /gs && git clone --recursive https://github.com/graphdeco-inria/gaussian-splatting .

RUN python3.10 -m pip install plyfile tqdm scikit-learn
RUN python3.10 -m pip install  https://huggingface.co/camenduru/gaussian-splatting/resolve/main/diff_gaussian_rasterization-0.0.0-cp310-cp310-linux_x86_64.whl
RUN python3.10 -m pip install  https://huggingface.co/camenduru/gaussian-splatting/resolve/main/simple_knn-0.0.0-cp310-cp310-linux_x86_64.whl
RUN python3.10 -m pip install torchvision




#also get add-ons
#git clone https://github.com/antimatter15/splat
#git clone https://github.com/ReshotAI/gaussian-splatting-blender-addon/

RUN git clone https://github.com/colmap/colmap
RUN mkdir colmap/build
WORKDIR colmap/build
RUN cmake .. -DCMAKE_CUDA_ARCHITECTURES="60;70;80" #-DCUDA_ENABLED=OFF 
RUN make -j8
RUN make install
WORKDIR /gs

RUN git clone https://github.com/francescofugazzi/3dgsconverter #needs scikit-learn

COPY run.sh /bin/

