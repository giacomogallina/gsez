# gsez: 3D Gaussian Splatting made Easy

3D Gaussian Splatting is a novel technique to represent a 3D scene, that is
easy to reconstruct from a set of images. See [here](https://github.com/graphdeco-inria/gaussian-splatting)
for the author's paper and reference implementation.

Unfortunately, while this research is really cool, installing a bunch
of python packages and cuda development libraries is really not.
gsez tries to solve this problem by bundling all this software in a docker image,
where everything should work, and to provide an easy to use cli that handles
the back and forth with the docker container.

## Building
gsez has the following requirements:
- python3
- ffmpeg
- docker
- nvidia container toolkit

To build gsez, just run
```
$ ./build.sh
```
This builds and tags a docker image with everything needed to reconstruct a 3D model from
an input video or image set.

## Running
Just run
```
$ ./gsez.py INPUT_VIDEO
```
or
```
$ ./gsez.py IMAGES_DIRECTORY
```
For more options, see
```
$ ./gsez.py -h
```


